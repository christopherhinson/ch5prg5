//Chris Hinson
//10/23/18
//Chapter 5 Program 5

public class main {
    public static void main(String[] args)
    {
        Output o = new Output();
        for (int i=1;i<=10;i++)
        {
            System.out.print(i + " seconds : ");
            o.print(fallingDistance(i), "Cyan");
        }
    }

    public static double fallingDistance(int fallingTime)
    {
        double fallingDistance  = .5*9.8*(fallingTime*fallingTime);
        return fallingDistance;
    }
}
