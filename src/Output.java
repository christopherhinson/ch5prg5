//Chris Hinson
//10/23/18
//This class lets me print in pretty colors

public class Output {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";


    public void print(String printString)
    {
        System.out.println(printString);
    }

    public void print(String printString, String color)
    {
        if (color.equals("Black")) {
            System.out.println(ANSI_BLACK + printString + ANSI_RESET);
        }
        else if (color.equals("Red"))
        {
            System.out.println(ANSI_RED + printString + ANSI_RESET);
        }
        else if (color.equals("Green"))
        {
            System.out.println(ANSI_GREEN + printString + ANSI_RESET);
        }
        else if (color.equals("Yellow"))
        {
            System.out.println(ANSI_YELLOW + printString + ANSI_RESET);
        }
        else if (color.equals("Blue"))
        {
            System.out.println(ANSI_BLUE + printString + ANSI_RESET);
        }
        else if (color.equals("Purple"))
        {
            System.out.println(ANSI_PURPLE + printString + ANSI_RESET);
        }
        else if (color.equals("Cyan"))
        {
            System.out.println(ANSI_CYAN + printString + ANSI_RESET);
        }
        else if (color.equals("White"))
        {
            System.out.println(ANSI_WHITE + printString + ANSI_RESET);
        }
    }

    //TODO: rename printString to Print"appropriate type"
    public void print (int printString, String color)
    {
        if (color.equals("Black")) {
            System.out.println(ANSI_BLACK + printString + ANSI_RESET);
        }
        else if (color.equals("Red"))
        {
            System.out.println(ANSI_RED + printString + ANSI_RESET);
        }
        else if (color.equals("Green"))
        {
            System.out.println(ANSI_GREEN + printString + ANSI_RESET);
        }
        else if (color.equals("Yellow"))
        {
            System.out.println(ANSI_YELLOW + printString + ANSI_RESET);
        }
        else if (color.equals("Blue"))
        {
            System.out.println(ANSI_BLUE + printString + ANSI_RESET);
        }
        else if (color.equals("Purple"))
        {
            System.out.println(ANSI_PURPLE + printString + ANSI_RESET);
        }
        else if (color.equals("Cyan"))
        {
            System.out.println(ANSI_CYAN + printString + ANSI_RESET);
        }
        else if (color.equals("White"))
        {
            System.out.println(ANSI_WHITE + printString + ANSI_RESET);
        }
    }

    public void print (double printString, String color)
    {
        if (color.equals("Black")) {
            System.out.println(ANSI_BLACK + printString + ANSI_RESET);
        }
        else if (color.equals("Red"))
        {
            System.out.println(ANSI_RED + printString + ANSI_RESET);
        }
        else if (color.equals("Green"))
        {
            System.out.println(ANSI_GREEN + printString + ANSI_RESET);
        }
        else if (color.equals("Yellow"))
        {
            System.out.println(ANSI_YELLOW + printString + ANSI_RESET);
        }
        else if (color.equals("Blue"))
        {
            System.out.println(ANSI_BLUE + printString + ANSI_RESET);
        }
        else if (color.equals("Purple"))
        {
            System.out.println(ANSI_PURPLE + printString + ANSI_RESET);
        }
        else if (color.equals("Cyan"))
        {
            System.out.println(ANSI_CYAN + printString + ANSI_RESET);
        }
        else if (color.equals("White"))
        {
            System.out.println(ANSI_WHITE + printString + ANSI_RESET);
        }
    }
}
